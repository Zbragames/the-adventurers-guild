local coroutineUtils = require("The-Adventurers-Guild/coroutine-utils")
local effectData = require("The-Adventurers-Guild/effect-data")
local characterUtils = require("The-Adventurers-Guild/character-utils")
local objectUtils = require("The-Adventurers-Guild/object-utils")

-- author: joe
-- used for adding and removing effect indicators above models in the game
local effectIndicator = {}

local activeIndicators = {}

-- Spacing between each indicator object on their parent's x-axis
local INDICATOR_SPACING = 0.1
-- Where an indicator is spawned relative to the top-middle of the object's bounds.
local INDICATOR_OFFSET = Vector(0,0.5,0)
local INDICATOR_OFFSET_WEBBED = Vector(0,0.9,0)

local INDICATOR_SCALE = Vector(0.175,0.175,0.175)

-- Effect names to indicator cube bundle links
local INDICATOR_ASSETS = {
    ["confuse"]="http://cloud-3.steamusercontent.com/ugc/1763691353664801924/53594AD78AA1DE8AC9B5C3A3A4AB65A20CE195B2/",
    ["iron_skin"]="http://cloud-3.steamusercontent.com/ugc/1763691353664812836/42957C23DED575A6E431166C708DC6A65E8B551D/",
    ["levitate"]="http://cloud-3.steamusercontent.com/ugc/1763691353664814362/132BB253F9F37A25E9252EBDA462D3376188FFDF/",
    ["life_force_heal"]="http://cloud-3.steamusercontent.com/ugc/1763691353664816526/5F945C7AF68968FDF991D7BD9171CC74D5519A3B/",
    ["life_force_track"]="http://cloud-3.steamusercontent.com/ugc/1763691353664816526/5F945C7AF68968FDF991D7BD9171CC74D5519A3B/",
    ["ogre_strength"]="http://cloud-3.steamusercontent.com/ugc/1763691353664821298/0823457D28CBD330D128100CE8D5EC34F3FF9085/",
    ["rebound"]="http://cloud-3.steamusercontent.com/ugc/1763691353664822319/C3EE11B11B8C34CC03A0ADFE581B5029FD814518/",
    ["shield"]="http://cloud-3.steamusercontent.com/ugc/1763691353665055701/108367618A3B806313D2C8143558F33B9D9D19A1/"
}


local function registerActiveIndicator(parentObjectGUID, effectName, indicatorObjectGUID)

    activeIndicators[parentObjectGUID] = activeIndicators[parentObjectGUID] or {}

    table.insert(activeIndicators[parentObjectGUID], {effectName=effectName, guid=indicatorObjectGUID})

end

local function unregisterActiveIndicator(parentObjectGUID, effectName)

    activeIndicators[parentObjectGUID] = activeIndicators[parentObjectGUID] or {}
    
    local removedIndicatorGUID = nil

    for i,indicatorEntry in ipairs(activeIndicators[parentObjectGUID]) do
        if indicatorEntry.effectName == effectName then
            removedIndicatorGUID = indicatorEntry.guid
            table.remove(activeIndicators[parentObjectGUID], i)
            break
        end
    end

    return removedIndicatorGUID

end

-- handles the creation of the indicator object
local function spawnIndicatorCoroutine(effectName, parentObject)
    
    local indicatorCubeData  = INDICATOR_ASSETS[effectName]

    local spawnParams = {
        type = "Custom_AssetBundle",
        sound = false,
    }
    local indicatorObject = spawnObject(spawnParams)
    local customParams = {
        assetbundle = indicatorCubeData
    }
    indicatorObject.setCustomObject(customParams)
    indicatorObject = indicatorObject.reload()

    coroutineUtils.yieldUntil(function() return indicatorObject.spawning == false and indicatorObject.loading_custom == false end)

    indicatorObject.tooltip = true
    indicatorObject.mass = 0
    indicatorObject.setGMNotes(string.format("%s_EffectIndicator", effectName))
    indicatorObject.setScale(INDICATOR_SCALE)

    local effectInfo = effectData[effectName]

    indicatorObject.setName(effectInfo.nameFormatted)
    indicatorObject.setDescription(effectInfo.description)

    for i,col in ipairs(indicatorObject.getComponents("Collider")) do
        col.set("isTrigger", true)
    end
    for i,col in ipairs(indicatorObject.getComponentsInChildren("Collider")) do
        col.set("isTrigger", true)
    end
    indicatorObject.getComponentInChildren("Rigidbody").set("isKinematic", true)

    registerActiveIndicator(parentObject.getGUID(), effectName, indicatorObject.getGUID())

    effectIndicator.updateIndicatorPositions(parentObject)

end

-- creates and registers a new active indicator on an object
function effectIndicator.addIndicator(effectName, parentObject)

    local indicatorCubeData = INDICATOR_ASSETS[effectName]

    if indicatorCubeData == nil then
        log(string.format("effectIndicator.addIndicator: No data associated with effect name: '%s'.", effectName), "error")
        return
    end

    coroutineUtils.launchAndReportErrors("effectIndicator.spawnIndicatorCoroutine", spawnIndicatorCoroutine, effectName, parentObject)

end

function effectIndicator.removeIndicator(effectName, parentObject)

    local indicatorObjectGUID = unregisterActiveIndicator(parentObject.getGUID(), effectName)
    
    if indicatorObjectGUID == nil then
        log(string.format("effectIndicator.removeIndicator: Attempted to remove indicator for '%s'. '%s' has no such indicator.", effectName, parentObject.getGUID()))
        return
    end

    local indicatorObject = getObjectFromGUID(indicatorObjectGUID)

    indicatorObject.destruct()

    effectIndicator.updateIndicatorPositions(parentObject)

end

function effectIndicator.addTemporaryIndicator(effectName, parentObject, untilRoundNumber, untilRoundPhase)

    coroutineUtils.launchAndReportErrors("effectIndicator.addTemproaryIndicator", function()
        effectIndicator.addIndicator(effectName, parentObject)
        coroutineUtils.yieldUntil(function() return ((_G.gameState_fxns.getRoundCount() == untilRoundNumber and _G.gameState_fxns.getRoundPhase() >= untilRoundPhase) or (_G.gameState_fxns.getRoundCount() > untilRoundNumber)) end)
        effectIndicator.removeIndicator(effectName, parentObject)
    end)

end

-- arranges the indicators on a given object into a line
function effectIndicator.updateIndicatorPositions(parentObject)

    activeIndicators[parentObject.getGUID()] = activeIndicators[parentObject.getGUID()] or {}

    local indicatorList = activeIndicators[parentObject.getGUID()]

    local indicatorCount = #indicatorList

    -- offset= ((n-1)*(x))/2 | where n = amount of indicators, x = horizontal spacing between each indicator
    local offset = (characterUtils.isWarrior(parentObject) and objectUtils.isWebbed(parentObject)) and INDICATOR_OFFSET_WEBBED or INDICATOR_OFFSET
    log("offset:")
    log(offset)
    local spacingOffsetLocal = (0.5*((indicatorCount-1)*INDICATOR_SPACING))*parentObject.getTransformRight():normalized()
    local constantOffsetLocal = parentObject.getTransformRight():normalized()*offset.x + parentObject.getTransformUp():normalized()*offset.y + parentObject.getTransformForward():normalized()*offset.z
    local spawnPos = Vector(parentObject.getPosition().x, parentObject.getBounds().center.y+(parentObject.getBounds().size.y/2), parentObject.getPosition().z) - (spacingOffsetLocal) + (constantOffsetLocal)

    for i,indicatorEntry in ipairs(indicatorList) do

        local indicatorObject = getObjectFromGUID(indicatorEntry.guid)
        indicatorObject.jointTo()

        indicatorObject.setPosition(spawnPos)
        indicatorObject.jointTo(parentObject, {
            type = "Fixed",
            collision = true,
            break_force = math.huge,
            break_torgue = math.huge
        })

        spawnPos = spawnPos + INDICATOR_SPACING*parentObject.getTransformRight()

    end

end

-- Forces a cleanup of stored indicator data and destroys any existing indicatorObjects.
-- Removes data for objects that are no longer in the game or if a guid is passed, a cleanup runs for only that object.
function effectIndicator.cleanup(parentGUID)

    if parentGUID ~= nil then
        for i,indicatorEntry in ipairs(activeIndicators[parentGUID] or {}) do
            local indicatorObject = getObjectFromGUID(indicatorEntry.guid)
            if indicatorObject ~= nil then indicatorObject.destruct() end
        end
        activeIndicators[parentGUID] = nil
        return
    end

    for parentGUID, indicatorList in pairs(activeIndicators) do
        if getObjectFromGUID(parentGUID) == nil then
            effectIndicator.cleanup(parentGUID)
        end
    end

end

return effectIndicator