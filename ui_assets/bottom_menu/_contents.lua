local genTable = require("The-Adventurers-Guild/ui_assets/_gen")
local path = "bottom_menu"
local contents = {
  "pinning",
  "attack",
}

return genTable(path, contents)
