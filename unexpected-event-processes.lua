local unexpectedEventProcesses = {}

local interactionSystem = require "The-Adventurers-Guild/interaction-system"
local coroutineUtils = require "The-Adventurers-Guild/coroutine-utils"
local characterUtils = require "The-Adventurers-Guild/character-utils"

interactionSystem.processNames.trapEvent = "Trap Event"
interactionSystem.processNames.portcullisEvent = "Portcullis Event"
interactionSystem.processNames.deadBodyEvent = "Dead Body Event"
interactionSystem.processNames.oldBonesEvent = "Old Bones Event"
interactionSystem.processNames.scorpionsEvent = "Scorpions Event"
interactionSystem.processNames.caveInEvent = "Cave-In Event"
interactionSystem.processNames.encounterEvent = "Encounter Event"

interactionSystem.processNames.scorpionsReaction = "Scorpions Reaction"

local function rollForEachColorAndGetLowest(player)
  local lowest = 7
  local lowestColor
  for _, v in ipairs(gameState.GameInfo.initiative_order) do
    player.broadcast(string.format("Roll for %s.", v), Color.fromString(v))
    local diceRolled = interactionSystem.yieldForDice(player, 1, 10)
    if not diceRolled then 
      player.broadcast("Roll timed out, cancelling process.", Color.Orange)
      return
    end
    local roll = interactionSystem.state[player.color].rolls[1]
    -- TODO: what if tie? currently we take the later one
    if roll <= lowest then
      lowest = roll
      lowestColor = v
    end
  end
  assert(lowestColor, "Could not find lowest rolling color.")
  return lowestColor
end

function unexpectedEventProcesses.trapProcessCoroutine(player)
  local triggerer = rollForEachColorAndGetLowest(player)

  local characterData = characterUtils.getCharacterData(Player[triggerer])
  if characterData == nil then
    broadcastToAll("TRAP PROCESS ERROR: Cannot find character stats.", Color(1, 0, 0))
    return
  end

  local triggererName = characterData.name
  if triggererName == "" then triggererName = triggerer end
  local triggererDisplayColor = Color.fromString(triggerer)
  broadcastToAll(string.format("%s has set off a trap!", triggererName), triggererDisplayColor)
  player.broadcast("Roll for result.", triggererDisplayColor)
  local diceRolled = interactionSystem.yieldForDice(player, 1, 20)
  if not diceRolled then 
    player.broadcast("Roll timed out, cancelling process.", Color.Orange)
    return
  end

  local roll = interactionSystem.state[player.color].rolls[1]

  if roll == 1 then
    broadcastToAll("Explosion!", triggererDisplayColor)
    player.broadcast("Roll for damage. (1D6)", triggererDisplayColor)
    local diceRolled = interactionSystem.yieldForDice(player, 1, 20)
    if not diceRolled then 
      player.broadcast("Roll timed out, cancelling process.", Color.Orange)
      return
    end

    local damage = interactionSystem.state[player.color].rolls[1]
    broadcastToAll(string.format(
      "Each player on this board section takes %d wounds, with no modifiers for Toughness or Armor. (Manual)",
      damage
    ), triggererDisplayColor)
  elseif roll >= 2 and roll <= 5 then
    broadcastToAll("A pit!", triggererDisplayColor)
    player.broadcast("Roll for damage. (2D6)", triggererDisplayColor)
    local diceRolled = interactionSystem.yieldForDice(player, 2, 20)
    if not diceRolled then 
      player.broadcast("Roll timed out, cancelling process.", Color.Orange)
      return
    end

    local tToughness = characterUtils.getStatTotal(characterData, "toughness")
    -- do players have armor?
    -- local tArmor = characterUtils.getStatTotal(targetData, "armor")

    local damage = sum(interactionSystem.state[player.color].rolls)
    damage = damage - tToughness -- - tArmor
    if damage < 0 then damage = 0 end
    local oldWounds = tonumber(characterData.health.current)
    local newWounds = oldWounds - damage

    broadcastToAll(string.format(
      "%s takes %d wounds from the fall.",
      triggererName,
      damage
    ), triggererDisplayColor)

    -- this is a string in current sheet version and I dont wanna break anything
    -- but really it should be a number
    characterData.health.current = tostring(newWounds)
    -- this will overwrite any changes made after we started and read char data
    characterUtils.setCharacterData(Player[triggerer], characterData)
  elseif roll == 6 then
    broadcastToAll("Treasure in the wall! Draw one Treasure card.", triggererDisplayColor)
  else
    assert(false)
  end

  player.broadcast("Roll for additional event card.", Color.White)
  local diceRolled = interactionSystem.yieldForDice(player, 1, 20)
  if not diceRolled then 
    player.broadcast("Roll timed out, cancelling process.", Color.Orange)
    return
  end

  if interactionSystem.state[player.color].rolls[1] <= 3 then
    broadcastToAll("Additional event! Draw another event card immediately.", Color.White)
  else
    broadcastToAll("No additional event.", Color.White)
  end
end

function unexpectedEventProcesses.portcullisProcessCoroutine(player)
  broadcastToAll("A portcullis closes behind you!", Color.White)
  broadcastToAll("Draw another event card immediately.", Color.White)
end

function unexpectedEventProcesses.deadBodyProcessCoroutine(player)
  local taker = rollForEachColorAndGetLowest(player)

  local characterData = characterUtils.getCharacterData(Player[taker])
  if characterData == nil then
    broadcastToAll("DEAD BODY PROCESS ERROR: Cannot find character stats.", Color(1, 0, 0))
    return
  end

  local takerName = characterData.name
  if takerName == "" then takerName = taker end
  local takerDisplayColor = Color.fromString(taker)

  broadcastToAll(string.format("%s has set off a trap!", takerName), takerDisplayColor)
  player.broadcast("Roll for result.", takerDisplayColor)
  local diceRolled = interactionSystem.yieldForDice(player, 1, 20)
  if not diceRolled then 
    player.broadcast("Roll timed out, cancelling process.", Color.Orange)
    return
  end

  local roll = interactionSystem.state[player.color].rolls[1]

  if roll == 1 then
    broadcastToAll("Poison Gas!", takerDisplayColor)
    player.broadcast("Roll for damage. (1D6)", takerDisplayColor)
    local diceRolled = interactionSystem.yieldForDice(player, 1, 20)
    if not diceRolled then 
      player.broadcast("Roll timed out, cancelling process.", Color.Orange)
      return
    end

    local damage = interactionSystem.state[player.color].rolls[1]
    broadcastToAll(string.format(
      "Each player on this board section takes %d wounds, with no modifiers for Toughness or Armor. (Manual)",
      damage
    ), takerDisplayColor)
  elseif roll >= 2 and roll <= 3 then
    broadcastToAll("Trap!", takerDisplayColor)
    player.broadcast("Roll for damage. (2D6)", takerDisplayColor)
    local diceRolled = interactionSystem.yieldForDice(player, 2, 20)
    if not diceRolled then 
      player.broadcast("Roll timed out, cancelling process.", Color.Orange)
      return
    end

    local tToughness = characterUtils.getStatTotal(characterData, "toughness")
    -- do players have armor?
    -- local tArmor = characterUtils.getStatTotal(targetData, "armor")

    local damage = sum(interactionSystem.state[player.color].rolls)
    damage = damage - tToughness -- - tArmor
    if damage < 0 then damage = 0 end
    local oldWounds = tonumber(characterData.health.current)
    local newWounds = oldWounds - damage

    broadcastToAll(string.format(
      "%s takes %d wounds from the trap.",
      takerName,
      damage
    ), takerDisplayColor)

    -- this is a string in current sheet version and I dont wanna break anything
    -- but really it should be a number
    characterData.health.current = tostring(newWounds)
    -- this will overwrite any changes made after we started and read char data
    characterUtils.setCharacterData(Player[taker], characterData)
  elseif roll >= 4 and roll <= 6 then
    broadcastToAll("Treasure!", takerDisplayColor)
    player.broadcast("Roll for gold.", takerDisplayColor)
    local diceRolled = interactionSystem.yieldForDice(player, 1, 20)
    if not diceRolled then 
      player.broadcast("Roll timed out, cancelling process.", Color.Orange)
      return
    end
    local value = interactionSystem.state[player.color].rolls[1] * 100
    broadcastToAll(string.format("The bag contains %d Gold.", value), takerDisplayColor)
  else
    assert(false)
  end

  broadcastToAll("Draw another event card immediately.", Color.White)
end

function unexpectedEventProcesses.oldBonesProcessCoroutine(player)
  player.broadcast("Roll for result.", Color.White)
  local diceRolled = interactionSystem.yieldForDice(player, 1, 20)
  if not diceRolled then 
    player.broadcast("Roll timed out, cancelling process.", Color.Orange)
    return
  end

  local roll = interactionSystem.state[player.color].rolls[1]

  if roll == 1 then
    broadcastToAll("Trap!", Color.White)

    -- TODO: autodetect from random player generator?
    player.broadcast("Please choose a warrior to take damage.")
    interactionSystem.state[player.color].target = nil
    local targetChosen = coroutineUtils.yieldUntil(
      function() return interactionSystem.state[player.color].target ~= nil end,
      20
    )
    if not targetChosen then
      broadcastToAll("Target selection timed out.", Color.Orange)
      return
    end
    local targetMini = interactionSystem.state[player.color].target
    if not characterUtils.isWarrior(targetMini) then
      player.broadcast("Warrior must have a [Warrior] keyword, cancelling.")
      return
    end

    local target = characterUtils.getMiniOwner(targetMini)

    local characterData = characterUtils.getCharacterData(target)
    if characterData == nil then
      broadcastToAll("OLD BONES PROCESS ERROR: Cannot find character stats.", Color(1, 0, 0))
      return
    end
  
    local targetName = characterData.name
    if targetName == "" then targetName = target.color end
    local targetDisplayColor = Color.fromString(target.color)

    player.broadcast("Roll for damage. (1D6)", targetDisplayColor)
    local diceRolled = interactionSystem.yieldForDice(player, 1, 20)
    if not diceRolled then 
      player.broadcast("Roll timed out, cancelling process.", Color.Orange)
      return
    end

    local damage = interactionSystem.state[player.color].rolls[1]
    if damage < 0 then damage = 0 end
    local oldWounds = tonumber(characterData.health.current)
    local newWounds = oldWounds - damage

    broadcastToAll(string.format(
      "%s takes %d wounds from the trap, with no modifiers for Toughness or Armor.",
      targetName,
      damage
    ), targetDisplayColor)

    -- this is a string in current sheet version and I dont wanna break anything
    -- but really it should be a number
    characterData.health.current = tostring(newWounds)
    -- this will overwrite any changes made after we started and read char data
    characterUtils.setCharacterData(target, characterData)
  elseif roll >= 2 and roll <= 3 then
    broadcastToAll("Illusion...", Color.White)
    broadcastToAll("Draw another event card immediately.", Color.White)
  elseif roll >= 4 and roll <= 5 then
    broadcastToAll("Gold!", Color.White)
    -- for _, v in ipairs(gameState.GameInfo.initiative_order) do
    --   player.broadcast(string.format("Roll Gold for %s. (1D6)", v), Color.fromString(v))
    --   local diceRolled = interactionSystem.yieldForDice(player, 1, 10)
    --   if not diceRolled then 
    --     player.broadcast("Roll timed out, cancelling process.", Color.Orange)
    --     return
    --   end
    --   local value = interactionSystem.state[player.color].rolls[1] * 10
    --   broadcastToAll(string.format("%s finds %d Gold.", v, value), Color.fromString(v))
    -- end
    broadcastToAll("Each player on this board section finds (1D6 x 10) Gold. (Manual)", Color.White)
    broadcastToAll("Draw another event card immediately.", Color.White)
  elseif roll == 6 then
    broadcastToAll("Treasure!", Color.White)
    -- for _, v in ipairs(gameState.GameInfo.initiative_order) do
    --   player.broadcast(string.format("Roll Gold for %s. (2D6)", v), Color.fromString(v))
    --   local diceRolled = interactionSystem.yieldForDice(player, 2, 10)
    --   if not diceRolled then 
    --     player.broadcast("Roll timed out, cancelling process.", Color.Orange)
    --     return
    --   end
    --   local value = interactionSystem.state[player.color].rolls[1] * 10
    --   broadcastToAll(string.format("%s finds %d Gold.", v, value), Color.fromString(v))
    -- end
    broadcastToAll("Each player on this board section finds (2D6 x 10) Gold. (Manual)", Color.White)
    broadcastToAll("Draw one Treasure Card.", Color.White)
  else
    assert(false)
  end
end

function unexpectedEventProcesses.scorpionsProcessCoroutine(player)
  -- TODO: autodetect from random player generator?
  player.broadcast("Please choose a warrior to be attacked.")
  interactionSystem.state[player.color].target = nil
  local targetChosen = coroutineUtils.yieldUntil(
    function() return interactionSystem.state[player.color].target ~= nil end,
    20
  )
  if not targetChosen then
    player.broadcast("Target selection timed out, cancelling.", Color.Orange)
    return
  end
  local targetMini = interactionSystem.state[player.color].target
  if not characterUtils.isWarrior(targetMini) then
    player.broadcast("Warrior must have a [Warrior] keyword, cancelling.")
    return
  end

  local target = characterUtils.getMiniOwner(targetMini)

  if target == player then
    -- jank process nesting
    interactionSystem.state[target.color].runningProcess = "scorpionsReaction"
    unexpectedEventProcesses.scorpionsReactionProcessCoroutine(target)
    interactionSystem.state[target.color].runningProcess = "scorpions"
  else
    local rp = interactionSystem.state[target.color].runningProcess
    if rp then
      player.broadcast(string.format(
        "%s can't roll while in %s process, cancelling.",
        target.color,
        interactionSystem.processNames[rp] or rp
      ), Color.Orange)
      return
    end
  
    -- launch seperate process for that player to do, wait for it to end
    unexpectedEventProcesses.launchScorpionsReactionProcess(target)
    coroutineUtils.yieldUntil(function()
      return not interactionSystem.state[target.color].runningProcess
    end)
  end

  player.broadcast("Roll for additional event card.", Color.White)
  local diceRolled = interactionSystem.yieldForDice(player, 1, 20)
  if not diceRolled then 
    player.broadcast("Roll timed out, cancelling process.", Color.Orange)
    return
  end

  if interactionSystem.state[player.color].rolls[1] <= 3 then
    broadcastToAll("Additional event! Draw another event card immediately.", Color.White)
  else
    broadcastToAll("No additional event.", Color.White)
  end
end

function unexpectedEventProcesses.caveInProcessCoroutine(player)
  broadcastToAll("Cave-in! All other exits are blocked!", Color.White)
  broadcastToAll("If this card is drawn in the first room of the dungeon, ignore it and draw another event card immediately.", Color.White)
end

function unexpectedEventProcesses.encounterProcessCoroutine(player)
  broadcastToAll("Players get the portcullis key.", Color.White)
  broadcastToAll("Draw another event card immediately.", Color.White)
end

--

function unexpectedEventProcesses.scorpionsReactionProcessCoroutine(player)
  local characterData = characterUtils.getCharacterData(player)
  if characterData == nil then
    broadcastToAll("SCORPIONS PROCESS ERROR: Cannot find character stats.", Color(1, 0, 0))
    return
  end

  local playerName = characterData.name
  if playerName == "" then playerName = player.color end
  local playerDisplayColor = Color.fromString(player.color)

  broadcastToAll(string.format("%s is attacked by scorpions!", playerName), playerDisplayColor)
  player.broadcast("Roll for damage. (1D6)", playerDisplayColor)
  local diceRolled = interactionSystem.yieldForDice(player, 1, 30)
  if not diceRolled then 
    player.broadcast("Roll timed out, cancelling process.", Color.Orange)
    return
  end

  local pStrength = characterUtils.getStatTotal(characterData, "strength")


  local damage = interactionSystem.state[player.color].rolls[1]
  damage = damage + pStrength
  if damage < 0 then damage = 0 end
  local scorpionsLeft = 12 - damage

  if scorpionsLeft <= 0 then
    broadcastToAll(string.format(
      "%s manages to slay all the scorpions!",
      playerName
    ), targetDisplayColor)
    return
  end

  broadcastToAll(string.format(
    "%s slays %d scorpions, but the survivors deal a total of %d Wounds, with no modifiers for Toughness or Armor.",
    playerName,
    damage,
    scorpionsLeft
  ), targetDisplayColor)

  local oldWounds = tonumber(characterData.health.current)
  local newWounds = oldWounds - damage

  -- this is a string in current sheet version and I dont wanna break anything
  -- but really it should be a number
  characterData.health.current = tostring(newWounds)
  -- this will overwrite any changes made after we started and read char data
  characterUtils.setCharacterData(player, characterData)
end

--

function unexpectedEventProcesses.launchTrapProcess(player)
  coroutineUtils.launchAndReportErrors(
    "unexpectedEventProcesses.trapProcessCoroutine",
    function()
      interactionSystem.state[player.color].runningProcess = "trapEvent"
      local st, res = pcall(function()
        unexpectedEventProcesses.trapProcessCoroutine(player)
      end)
      interactionSystem.cleanup(player)
      if not st then error(res) end
    end
  )
end

function unexpectedEventProcesses.launchPortcullisProcess(player)
  coroutineUtils.launchAndReportErrors(
    "unexpectedEventProcesses.portcullisProcessCoroutine",
    function()
      interactionSystem.state[player.color].runningProcess = "portcullisEvent"
      local st, res = pcall(function()
        unexpectedEventProcesses.portcullisProcessCoroutine(player)
      end)
      interactionSystem.cleanup(player)
      if not st then error(res) end
    end
  )
end

function unexpectedEventProcesses.launchDeadBodyProcess(player)
  coroutineUtils.launchAndReportErrors(
    "unexpectedEventProcesses.deadBodyProcessCoroutine",
    function()
      interactionSystem.state[player.color].runningProcess = "deadBodyEvent"
      local st, res = pcall(function()
        unexpectedEventProcesses.deadBodyProcessCoroutine(player)
      end)
      interactionSystem.cleanup(player)
      if not st then error(res) end
    end
  )
end

function unexpectedEventProcesses.launchOldBonesProcess(player)
  coroutineUtils.launchAndReportErrors(
    "unexpectedEventProcesses.oldBonesProcessCoroutine",
    function()
      interactionSystem.state[player.color].runningProcess = "oldBonesEvent"
      local st, res = pcall(function()
        unexpectedEventProcesses.oldBonesProcessCoroutine(player)
      end)
      interactionSystem.cleanup(player)
      if not st then error(res) end
    end
  )
end

function unexpectedEventProcesses.launchOldBonesProcess(player)
  coroutineUtils.launchAndReportErrors(
    "unexpectedEventProcesses.oldBonesProcessCoroutine",
    function()
      interactionSystem.state[player.color].runningProcess = "oldBonesEvent"
      local st, res = pcall(function()
        unexpectedEventProcesses.oldBonesProcessCoroutine(player)
      end)
      interactionSystem.cleanup(player)
      if not st then error(res) end
    end
  )
end

function unexpectedEventProcesses.launchScorpionsProcess(player)
  coroutineUtils.launchAndReportErrors(
    "unexpectedEventProcesses.scorpionsProcessCoroutine",
    function()
      interactionSystem.state[player.color].runningProcess = "scorpionsEvent"
      local st, res = pcall(function()
        unexpectedEventProcesses.scorpionsProcessCoroutine(player)
      end)
      interactionSystem.cleanup(player)
      if not st then error(res) end
    end
  )
end

function unexpectedEventProcesses.launchCaveInProcess(player)
  coroutineUtils.launchAndReportErrors(
    "unexpectedEventProcesses.caveInProcessCoroutine",
    function()
      interactionSystem.state[player.color].runningProcess = "caveInEvent"
      local st, res = pcall(function()
        unexpectedEventProcesses.caveInProcessCoroutine(player)
      end)
      interactionSystem.cleanup(player)
      if not st then error(res) end
    end
  )
end

function unexpectedEventProcesses.launchEncounterProcess(player)
  coroutineUtils.launchAndReportErrors(
    "unexpectedEventProcesses.encounterProcessCoroutine",
    function()
      interactionSystem.state[player.color].runningProcess = "encounterEvent"
      local st, res = pcall(function()
        unexpectedEventProcesses.encounterProcessCoroutine(player)
      end)
      interactionSystem.cleanup(player)
      if not st then error(res) end
    end
  )
end

--

function unexpectedEventProcesses.launchScorpionsReactionProcess(player)
  coroutineUtils.launchAndReportErrors(
    "unexpectedEventProcesses.scorpionsReactionProcessCoroutine",
    function()
      interactionSystem.state[player.color].runningProcess = "scorpionsReaction"
      local st, res = pcall(function()
        unexpectedEventProcesses.scorpionsReactionProcessCoroutine(player)
      end)
      interactionSystem.cleanup(player)
      if not st then error(res) end
    end
  )
end

--

return unexpectedEventProcesses
