local objectUtils = require "The-Adventurers-Guild/object-utils"
local coroutineUtils = require "The-Adventurers-Guild/coroutine-utils"

local CLASS_STARTER_CHESTS_ID = "70d8f0"
local classStarterChest

local CHEST_AREA_IDS = {
  Blue   = "a3a98f",
  Teal   = "95fe5a",
  Yellow = "4fd69b",
  White  = "7e1b21",
  Green  = "ce8fbb",
  Orange = "62cbc8",
}
local chestAreas



local SHEET_CAST_POSITIONS = {
  Blue   = Vector(-24, 5, -61),
  Teal   = Vector( 24, 5, -61),
  Yellow = Vector( 79, 5, -24),
  White  = Vector( 79, 5,  24),
  Green  = Vector( 24, 5,  61),
  Orange = Vector(-24, 5,  61),
}
_G['SHEET_CAST_POSITIONS'] = deepcopy(SHEET_CAST_POSITIONS)
--

local characterUtils = {}

function characterUtils.initialize()
  classStarterChest = getObjectRefs(CLASS_STARTER_CHESTS_ID)
  chestAreas = getObjectRefs(CHEST_AREA_IDS)
end

function characterUtils.isWarrior(mini)
  local keywords = objectUtils.getKeywords(mini.getDescription())
  return keywords["warrior"] ~= nil
end

-- author: joe3012
-- returns true if the player has more than 0 HP
function characterUtils.isAlive(player)

  local sheet = characterUtils.findSheet(player)

  if sheet == nil then log(string.format("characterUtils.isAlive: Couldn't find a character sheet for %s", player.color), "error") return end

  local health = tonumber(sheet.getTable("data").health.current)

  return health > 0

end

function characterUtils.getMiniOwner(mini)
  local keywords = objectUtils.getKeywords(mini.getDescription())
  return Player[keywords["warrior"]]
end

function characterUtils.getMini(player)
  local color = string.lower(player.color)
  -- this is so inefficient. once have more robust state we can replace this
  --
  --counterproposal just cache it each time and just check to make sure it hasn't gone nil : the fool
  for _, object in ipairs(getAllObjects()) do
    if object.type == "Figurine" then
      local keywords = objectUtils.getKeywords(object.getDescription())
      if keywords["warrior"] == color then return object end
    end
  end
  return nil
end

function characterUtils.createStarterCharacter(player, class)
  local data = characterUtils.getStarterChestDataForClass(class)
  local area = characterUtils.getChestArea(player)
  spawnObjectData({
    data              = data,
    position          = area.getPosition() + Vector(0, 1.1, 0),
    rotation          = area.getRotation(),
    callback_function = function(obj)
      obj.setLock(true)
      local afterLoaded = function() obj.call("load_click") end
      Wait.condition(
        afterLoaded,
        function() return not obj.loading_custom end,
        10,
        function()
          log("Chest asset loading timeout, maybe behave oddly.")
          afterLoaded()
        end
      )
    end,
  })
end

function characterUtils.getStarterChestDataForClass(class)
  local chests = classStarterChest.getData().ContainedObjects
  for _, data in ipairs(chests) do
    if data.Nickname == class then
      return data
    end
  end
end

function characterUtils.getChestArea(player)
  return chestAreas[player.color]
end

function characterUtils.getAllNotes(data)
  local acc = ""
  for _, card in ipairs(data.notes) do
    for _, page in ipairs(card) do
      acc = acc.."\n"..page.body
    end
  end
  return acc
end

function characterUtils.findChest(player)
  for _, hit in ipairs(Physics.cast({
    origin       = characterUtils.getChestArea(player).getPosition(),
    direction    = Vector(0, 1, 0),
    max_distance = 5,
    type         = 1,
  })) do
    local buttons = hit.hit_object.getButtons()
    if buttons ~= nil and #buttons == 2 and buttons[1].label == "Save" and buttons[2].label == "Load" then
      return hit
    end
  end
  return nil
end

function characterUtils.findSheet(player)
  assert(player~=nil and player.color~=nil,"findSheet: player.color entry required:" .. logString(player))
  for _, hit in ipairs(Physics.cast({
    origin       = SHEET_CAST_POSITIONS[player.color],
    direction    = Vector(0, -1, 0),
    max_distance = 10,
    type         = 1,
  })) do
    local inputs = hit.hit_object.getInputs()
    if inputs ~= nil and #inputs >= 2 and hit.hit_object.script_state ~= nil and hit.hit_object.script_state ~= "" then
      return hit.hit_object
    end
  end
  return nil
end

function characterUtils.getCharacterData(player)
  if characterUtils.findSheet(player)~=nil then
    return characterUtils.findSheet(player).getTable("data")
  else
    return nil
  end
end

function characterUtils.setCharacterData(player, data)
  local sheet = characterUtils.findSheet(player)
  sheet.setTable("data", data)
  sheet.call("updateSheet")
  sheet.call("updateGameState")
end

function characterUtils.getStatTotal(data, stat)
  return data.attributes[stat].base + data.attributes[stat].modifier
end

-- DEPRECATED: use characterUtils.getCharacterData(player).name
function characterUtils.getCharacterName(player)
  local sheet = characterUtils.findSheet(player)
  if sheet == nil then
    return nil
  end
  local inputs = sheet.getInputs()
  if #inputs >= 3 then
    if inputs[3].value == "" then
      return nil
    else
      return inputs[3].value
    end
  end
  return nil
end

-- returns either the player's name or class depending on what has been set in the character sheet
-- returns formatted with player's colour by default
-- author: joe3012
function characterUtils.getDisplayName(player, returnFormatted)

  if returnFormatted == nil then returnFormatted = true end

  local characterData = characterUtils.getCharacterData(player)
  local name = characterData.name
  local class = characterData.class

  local displayName = (string.len(name) ~= 0) and name or class
  displayName = string.format("%s(%s)", displayName, player.color)
  
  if returnFormatted == true then
    displayName = string.format("[%s]%s[-]", Color.fromString(player.color):toHex(), displayName)
  end

  return displayName

end


-- DEPRECATED: use characterUtils.getCharacterData(player).class
function characterUtils.getClass(player)
  local sheet = characterUtils.findSheet(player)
  if sheet == nil then
    return ""
  end
  local inputs = sheet.getInputs()
  if #inputs >= 6 then
    return inputs[6].value
  end
  return ""
end

function characterUtils.getEquippedItems(player)
  local sheet = characterUtils.findSheet(player)
  local items = {}
  for id, v in pairs(sheet.getTable("data").equipped_items) do
    if v then table.insert(items, getObjectFromGUID(id)) end
  end
  return items
end

-- this should really be folded into objectUtils.getStats()
function characterUtils.getWeaponStats(obj)
  local desc = obj.getDescription()
  local damage = string.match(desc, "%f[%w]WD%s*([%+%-]?%d+)")  -- WD
  if not damage then
    return nil
  end
  return {
    damage = tonumber(damage) or 0,
  }
end

function characterUtils.applyItemStatMods(item, player)
  local mods = objectUtils.getStats(item.getDescription())
  if not mods then return end
  local sheet = characterUtils.findSheet(player)
  local data = sheet.getTable("data")

  for k, v in pairs(mods) do
    if data.attributes[k] then
      data.attributes[k].modifier = data.attributes[k].modifier + v
    end
  end

  sheet.setTable("data", data)
  sheet.call("updateSheet")
end

function characterUtils.unapplyItemStatMods(item, player)
  local mods = objectUtils.getStats(item.getDescription())
  if not mods then return end
  local sheet = characterUtils.findSheet(player)
  local data = sheet.getTable("data")

  for k, v in pairs(mods) do
    data.attributes[k].modifier = data.attributes[k].modifier - v
  end

  sheet.setTable("data", data)
  sheet.call("updateSheet")
end

function characterUtils.addTemporaryStatChange(player, deltaStats, untilRoundNumber, untilRoundPhase)

  local sheet = characterUtils.findSheet(player)

  if sheet == nil then
    error(string.format("Unable to find a sheet for %s", player.color))
    return
  end

  assert(tonumber(untilRoundPhase), "untilRoundPhase must be passed as an integer")

  -- add value to stat modifier

  local sheetData = sheet.getTable("data")

  for statName, statChange in pairs(deltaStats) do
    sheetData.attributes[statName].modifier = (sheetData.attributes[statName].modifier + statChange)
    log(string.format("Changing %s's %s stat by %d until phase %d of round %d", player.color, statName, statChange, untilRoundPhase, untilRoundNumber))
  end
  sheet.setTable("data", sheetData)
  sheet.call("updateSheet", stat)


  -- wait until desired roundNumber and turnPhase

  coroutineUtils.launchAndReportErrors("addTemporaryStatChangeCoroutine (Warrior)", function()
    
    coroutineUtils.yieldUntil(function() 
    
      return ((_G.gameState_fxns.getRoundCount() == untilRoundNumber and _G.gameState_fxns.getRoundPhase() >= untilRoundPhase) or (_G.gameState_fxns.getRoundCount() > untilRoundNumber))

    end)

    -- undo stat changes

    -- get sheet again, incase it has moved
    sheet = characterUtils.findSheet(player)

    if sheet == nil then
      log(string.format("Unable to find a sheet for %s", player.color))
      return
    end

    sheetData = sheet.getTable("data")

    for statName, statChange in pairs(deltaStats) do
      sheetData.attributes[statName].modifier = (sheetData.attributes[statName].modifier - statChange)
      log(string.format("Changing %s's %s stat by %d", player.color, statName, (-statChange)))
    end
    sheet.setTable("data", sheetData)
    sheet.call("updateSheet", stat)


  end)

end
--Debugger
_G["cU"] = characterUtils
return characterUtils
