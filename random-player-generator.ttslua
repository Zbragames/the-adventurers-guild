-- author: joe3012
local characterUtils = require("The-Adventurers-Guild/character-utils")
local coroutineUtils = require("The-Adventurers-Guild/coroutine-utils")

local randomPlayerGenerator = {}

randomPlayerGenerator.tokenGUIDS = {}

-- flag used to see if the system is currently displaying a randomly generated player
randomPlayerGenerator.runningGenerationProc = false

-- position and rotation for decorative character mini spawning
local MODEL_SPAWN_POSITION = {76.05, 3.00, 62.82}
local MODEL_SPAWN_ROTATION = {0.00, 0.00, 0.00}

-- the amount of time in seconds that the decorative character mini remains above the pit
local MODEL_SPAWN_DELAY_SECONDS = 2

-- each player's presence in the random player
-- generator's pool, from which it draws a
-- random Player
local playerColorPool = 
{
    ["Blue"] = false,
    ["Teal"] = false,
    ["Yellow"] = false,
    ["White"] = false,
    ["Orange"] = false,
    ["Green"] = false
}

-- TODO: change this to update using getCustomAssets (like in the treasure buttons)
--image links for each warrior class
local assetLinks = {
    ["none_character"] = "https://i.imgur.com/JfiNZkd.png",
    ["barbarian"] = "https://raw.githubusercontent.com/Fly-n-dad/The-Adventurers-Guild/save,-character-menus/ui_assets/class_menu/barbarian.png",
    ["wizard"] = "https://raw.githubusercontent.com/Fly-n-dad/The-Adventurers-Guild/save,-character-menus/ui_assets/class_menu/wizard.png",
    ["dwarf"] = "https://raw.githubusercontent.com/Fly-n-dad/The-Adventurers-Guild/save,-character-menus/ui_assets/class_menu/dwarf.png",
    ["elf"] = "https://raw.githubusercontent.com/Fly-n-dad/The-Adventurers-Guild/save,-character-menus/ui_assets/class_menu/elf.png",
    ["pit fighter"] = "https://raw.githubusercontent.com/Fly-n-dad/The-Adventurers-Guild/save,-character-menus/ui_assets/class_menu/pit_fighter.png",
    ["wardancer"] = "https://raw.githubusercontent.com/Fly-n-dad/The-Adventurers-Guild/save,-character-menus/ui_assets/class_menu/wardancer.png",
    ["witch hunter"] = "https://raw.githubusercontent.com/Fly-n-dad/The-Adventurers-Guild/save,-character-menus/ui_assets/class_menu/witch_hunter.png",
    ["imperial noble"] = "https://raw.githubusercontent.com/Fly-n-dad/The-Adventurers-Guild/save,-character-menus/ui_assets/class_menu/imperial_noble.png"
}

-- run when a player is generated
local function randomPlayerGenerationCoroutine(clickPlayer)

    -- flag that prevents this coroutine from runing
    -- more than once at the same time
    randomPlayerGenerator.runningGenerationProc = true

    local randomColor = randomPlayerGenerator.getRandomPlayerColorFromPool()

    if randomColor == nil then
        clickPlayer.broadcast("There are no players in the pool! Click on the tokens next to the pit!", Color.fromString("Yellow"))
        randomPlayerGenerator.runningGenerationProc = false
        return
    end

    local randomPlayer = Player[randomColor]
    local characterData = characterUtils.getCharacterData(randomPlayer)

    if characterData ~= nil then
        local playerName = characterData.name
        broadcastToAll(string.format("Fate has chosen [%s]%s(%s)[%s]!", Color.fromString(randomColor):toHex(), playerName, randomColor, Color.fromString(randomColor):toHex()), Color.fromString("Yellow"))

        local playerMini = characterUtils.getMini(randomPlayer)

        if playerMini ~= nil then

            -- spawn a clone of the player's model above the pit
            local miniClone = playerMini.clone({position=MODEL_SPAWN_POSITION})
            miniClone.setRotation(MODEL_SPAWN_ROTATION)
            miniClone.setDescription("")
            miniClone.setName("")
            miniClone.setLock(true)
            miniClone.interactable = false

            coroutineUtils.yieldSeconds(MODEL_SPAWN_DELAY_SECONDS)

            -- delete after a few seconds
            miniClone.destruct()

        end

    else
        broadcastToAll(string.format("Fate has chosen [%s]%s[%s]!", Color.fromString(randomColor):toHex(), randomColor, Color.fromString(randomColor):toHex()), Color.fromString("Yellow"))
    end
    

    randomPlayerGenerator.runningGenerationProc = false
    return

end

-- helper function for changing a player's presence in the random
-- player generator pool
function randomPlayerGenerator.setPoolState(color, state)

    playerColorPool[color] = state

    local tokenObject = getObjectFromGUID(randomPlayerGenerator.tokenGUIDS[color])

    if state == true then
        tokenObject.highlightOn(Color.fromString(color))
    else
        tokenObject.highlightOff()
    end

end

--====== zaps RngPGs back to none player state on game init =====
--author: romanticfool
function randomPlayerGenerator.initialize()
end

-- called from tokens when they are clicked on
function randomPlayerGenerator.tokenClick(params)
    local color = params.color
    local newState = not playerColorPool[color]

    playerColorPool[color] = newState

    randomPlayerGenerator.setPoolState(color, newState)
end

-- called from the decorative pit when any player clicks it
function randomPlayerGenerator.pitButtonClick(params)
    local clickPlayer = Player[params.color]

    if randomPlayerGenerator.runningGenerationProc == false then
        coroutineUtils.launchAndReportErrors("randomPlayerGenerationCoroutine", randomPlayerGenerationCoroutine, clickPlayer)
    end
end

-- called from tokens so that we can keep track of their GUIDs
-- this is done instead of hard-coding because tokens will reload after setting their images, meaning
-- their GUIDS will change
function randomPlayerGenerator.registerTokenGUID(params)
    -- the color the token represents (Blue, Teal, Yellow, White, Green or Orange)
    local color = params.color
    local GUID = params.GUID

    randomPlayerGenerator.tokenGUIDS[color] = GUID

    randomPlayerGenerator.updateTokens(color)
end

-- returns a random player drawn from the random generator player pool
-- or nil if no players are present in the pool
function randomPlayerGenerator.getRandomPlayerColorFromPool()
    local playerColorsInPool = {}

    for playerColor, toggled in pairs(playerColorPool) do
        if toggled == true then
            table.insert(playerColorsInPool, playerColor)
        end
    end

    -- assuming it would be overkill to seed?
    return (#playerColorsInPool > 0) and playerColorsInPool[math.random(1,#playerColorsInPool)] or nil
end

function randomPlayerGenerator.setTokenImage(playerColor, url)
    local token = getObjectFromGUID(randomPlayerGenerator.tokenGUIDS[playerColor])
    token.call("setImage", {["url"]=url})
end

function randomPlayerGenerator.setTokenLabel(playerColor, labelString)
    log(labelString)
    local token = getObjectFromGUID(randomPlayerGenerator.tokenGUIDS[playerColor])
    log(token)
    token.call("setLabel", {labelString})
end

-- TODO: find a way to automatically update tokens when a sheet is removed from the table (by saving the chest most likely)
-- currently bugged! token object doesn't reload fast enough for the highlight to work, gets updated when a player changes portrait or character name...
function randomPlayerGenerator.updateTokens(color_)
    for color, tokenGUID in next, randomPlayerGenerator.tokenGUIDS do
        if (color_==nil) or (color==color_) then
            local tokenObj = getObjectFromGUID(tokenGUID)
            local characterData = characterUtils.getCharacterData(Player[color])

            if characterData ~= nil then
                local characterClass = characterData.class
                local characterName = characterData.name
                local imageLink = assetLinks[characterClass:lower()] or assetLinks["none_character"]

                if characterData.portrait_asset~=nil then
                    imageLink = characterData.portrait_asset.url~=nil and characterData.portrait_asset.url or imageLink
                end

                local tokenLabel

                if #characterName > 0 then
                   tokenLabel = string.format("[%s]%s(%s)[-]", Color.fromString(color):toHex(), characterName, characterClass)
                elseif #characterClass > 0 then 
                    tokenLabel = string.format("[%s]%s[-]", Color.fromString(color):toHex(), characterClass)
                else
                    tokenLabel = string.format("[%s]%s[-]", Color.fromString(color):toHex(), color)
                end

                randomPlayerGenerator.setTokenImage(color, imageLink)
                randomPlayerGenerator.setTokenLabel(color, tokenLabel)

                randomPlayerGenerator.setPoolState(color, true)
            else
                randomPlayerGenerator.setTokenImage(color, assetLinks["none_character"])
                randomPlayerGenerator.setPoolState(color, false)
            end
        end
    end
end
-- global interface for the generator
_G.randomPlayerGenerator_tokenClick = randomPlayerGenerator.tokenClick
_G.randomPlayerGenerator_pitButtonClick = randomPlayerGenerator.pitButtonClick
_G.randomPlayerGenerator_registerTokenGUID = randomPlayerGenerator.registerTokenGUID

return randomPlayerGenerator