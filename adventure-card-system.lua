local objectUtils = require "The-Adventurers-Guild/object-utils"
local interactionSystem = require "The-Adventurers-Guild/interaction-system"
local coroutineUtils = require "The-Adventurers-Guild/coroutine-utils"
local unexpectedEventProcesses = require "The-Adventurers-Guild/unexpected-event-processes"
local monsterUtils = require("The-Adventurers-Guild/monster-utils")

-- Dungeon and event deck automation, including initialization, activation, and
-- discarding.

local adventureCardSystem = {}

local DUNGEON_DECK_ZONE_ID = "ce13f1"
local dungeonDeckZone

local DUNGEON_DISCARD_ZONE_ID = "c24bf9"
local dungeonDiscardZone

local EVENT_DECK_ZONE_ID = "272ca0"
local eventDeckZone

local EVENT_DISCARD_ZONE_ID = "02bfa7"
local eventDiscardZone

local INITIAL_OBJECTIVE_ROOM_DECK_ID = "f34068"
local initialObjectiveRoomDeck

local INITIAL_DUNGEON_ROOM_DECK_ID = "4ea365"
local initialDungeonRoomDeck

local ACTIVATION_AREA_ID = "c43fd3"
activationArea = nil

local MONSTER_SPAWN_AREA_ID = "0d6220"
local monsterSpawnArea

local MONSTER_CHEST_ID = "8dfedf"
local monsterChest

--

function adventureCardSystem.initialize()
  dungeonDeckZone = getObjectRefs(DUNGEON_DECK_ZONE_ID)
  dungeonDiscardZone = getObjectRefs(DUNGEON_DISCARD_ZONE_ID)
  eventDeckZone = getObjectRefs(EVENT_DECK_ZONE_ID)
  eventDiscardZone = getObjectRefs(EVENT_DISCARD_ZONE_ID)
  initialObjectiveRoomDeck = getObjectRefs(INITIAL_OBJECTIVE_ROOM_DECK_ID)
  initialDungeonRoomDeck = getObjectRefs(INITIAL_DUNGEON_ROOM_DECK_ID)
  activationArea = getObjectRefs(ACTIVATION_AREA_ID)
  monsterSpawnArea = getObjectRefs(MONSTER_SPAWN_AREA_ID)
  monsterChest = getObjectRefs(MONSTER_CHEST_ID)

  -- objects must be registered to emit collision events to global
  activationArea.registerCollisions()
end

function adventureCardSystem.onObjectCollisionEnter(object, collision_info)
  if object ~= activationArea then return end
  local other = collision_info.collision_object
  if other.tag == "Card" and not other.is_face_down then
    adventureCardSystem.activateCard(other)
  end
end

function adventureCardSystem.putIntoObjectiveRoomDeck(card)
  initialObjectiveRoomDeck.putObject(card)
end
--create and place dungeon deck, set aside other decks.
function adventureCardSystem.setupForAdventure()
  local dungeonDeck = adventureCardSystem.makeDungeonDeck(gameState.Adventure.room:lower())

  -- not ideal, but sets them both aside.
  if initialObjectiveRoomDeck then
    initialDungeonRoomDeck.putObject(initialObjectiveRoomDeck)
  end

  spawnObjectData({
    data = dungeonDeck,
    position = dungeonDeckZone.getPosition(),
    rotation = dungeonDeckZone.getRotation() + Vector(180, 0, 0),
  })
  broadcastToAll(
    string.format("%s dungeon deck created.", gameState.Adventure.room),
    Color.White
  )
end


-- create a 13-card dungeon deck, with 6 dungeon room cards on top of 6 more
-- dungeon room cards mixed with one objective card and return its data.
-- objectiveRoomOrName - name of an objective room in the objective room deck or
-- data for its card directly
function adventureCardSystem.makeDungeonDeck(objectiveOrCard)
  -- #TODO: eventually we need to swap out explicit decks for a better system to
  -- easily allow cards beyond the core set
  local dungeonPalletData = initialDungeonRoomDeck.getData()
  
  local objectiveRoom
  if type(objectiveOrCard) == "string" then
    objectiveRoom = adventureCardSystem.getCardForObjective(objectiveOrCard)
  elseif type(objectiveOrCard) == "table" then
    objectiveRoom = objectiveOrCard
  else
    error(string.format("Invalid objective room or name: %s", objectiveOrCard))
  end

  -- TTS handles custom decks in a stupid way
  local objectiveCustomId = math.floor(objectiveRoom.CardID / 100)
  local objectiveCustomDeck = objectiveRoom.CustomDeck[objectiveCustomId]

  local cards = {}
  local numInPallet = #dungeonPalletData.ContainedObjects
  assert(numInPallet >= 12, string.format("Not enough dungeon cards (need at least 12, found %d)", numInPallet))
  for i = 1, 12 do
    -- we make the window smaller and take the chosen card out each time to remain unique
    local r = math.random(i, numInPallet)
    cards[i] = dungeonPalletData.ContainedObjects[r]
    -- mutating this is fine since getData is a copy and we're replacing this below anyways
    dungeonPalletData.ContainedObjects[r] = dungeonPalletData.ContainedObjects[i]
  end

  -- put objective room in bottom 7
  local r = math.random(7, 13)
  cards[13] = cards[r]
  cards[r] = objectiveRoom

  local dungeonDeck = dungeonPalletData
  dungeonDeck.ContainedObjects = cards
  -- dumb redundant data TTS needs for cards to work
  dungeonDeck.CustomDeck[objectiveCustomId] = objectiveCustomDeck
  dungeonDeck.DeckIDs = {}
  for i, v in ipairs(cards) do
    dungeonDeck.DeckIDs[i] = v.CardID
  end

  dungeonDeck.Nickname = "Dungeon Deck"
  return dungeonDeck
end

function adventureCardSystem.getCardForObjective(objective)
  -- #TODO: eventually we need to swap out explicit decks for a better system to
  -- easily allow cards beyond the core set
  local objectivePalletData = initialObjectiveRoomDeck.getData()
  for _, v in ipairs(objectivePalletData.ContainedObjects) do
    local keywords = objectUtils.getKeywords(v.Description)
    if keywords["objective_room"]:lower() == string.lower(objective) then
      return v
    end
  end
  error(string.format("No card for the objective \"%s\" found", objective))
end


function adventureCardSystem.activateCard(card)
  local keywords = objectUtils.getKeywords(card)
  local activator = gameState_fxns.getLastHolder(card)
  if not activator then return end

  if not gameState.Adventure.room and keywords["objective_room"] then
    gameState.Adventure.room = keywords["objective_room"]
    if random_adventure_coroutine~=true then
      initialObjectiveRoomDeck.putObject(card)
      adventureCardSystem.setupForAdventure()
    end
  elseif keywords["spawn room"] then
    broadcastToAll("Use PEEK (default: [Shift+Alt]) to spawn dungeon tiles.", Color.Orange)
  elseif keywords["spawn monster"] then
    adventureCardSystem.putInDeckOrZone(card, eventDiscardZone)

    local rp = interactionSystem.state[activator.color].runningProcess
    if rp then
      activator.broadcast(string.format(
        "Cannot spawn monsters while in %s process.",
        interactionSystem.processNames[rp] or rp
      ), Color.Orange)
      return
    end

    adventureCardSystem.launchSpawnMonstersProcess(
      activator,
      adventureCardSystem.parseSpawnMonsterParameter(keywords["spawn monster"])
    )
  elseif keywords["event"] then
    adventureCardSystem.putInDeckOrZone(card, eventDiscardZone)
    local event = keywords["event"]

    local rp = interactionSystem.state[activator.color].runningProcess
    if rp then
      activator.broadcast(string.format(
        "Cannot activate event while in %s process.",
        interactionSystem.processNames[rp] or rp
      ), Color.Orange)
      return
    end

    if event == "trap" then
      unexpectedEventProcesses.launchTrapProcess(activator)
    elseif event == "portcullis" then
      unexpectedEventProcesses.launchPortcullisProcess(activator)
    elseif event == "dead body" then
      unexpectedEventProcesses.launchDeadBodyProcess(activator)
    elseif event == "old bones" then
      unexpectedEventProcesses.launchOldBonesProcess(activator)
    elseif event == "scorpions" then
      unexpectedEventProcesses.launchScorpionsProcess(activator)
    elseif event == "cave-in" then
      unexpectedEventProcesses.launchCaveInProcess(activator)
    elseif event == "encounter" then
      unexpectedEventProcesses.launchEncounterProcess(activator)
    else
      activator.broadcast(string.format("Unrecognized event \"%s\", cannot be activated.", event), Color.Orange)
    end
  else
    activator.broadcast("Unrecognized card, cannot be activated.", Color.Orange)
  end
end

function adventureCardSystem.putInDeckOrZone(card, zone)
  local found = false
  for _, v in ipairs(zone.getObjects()) do
    if v.tag == "Card" or v.tag == "Deck" then
      found = true
      v.putObject(card)
      break
    end
  end

  if not found then
    card.setPosition(zone.getPosition())
    card.setRotation(zone.getRotation())
  end
end

function adventureCardSystem.parseSpawnMonsterParameter(text)
  local rolls = {}
  for snippet in string.gmatch(text, "([^&]+)") do
    local amount, dice, monster
    -- 1d6 minotaur
    amount, dice, monster = string.match(snippet, "^%s*(%d+)[Dd](%d+)%s*(.-)%s*$")
    if not monster then
      -- 1 minotaur
      amount, monster = string.match(snippet, "^%s*(%d+)%s*(.-)%s*$")
      if not monster then
        error("Can't understand Spawn Monster parameter: \""..snippet.."\"")
      end
    end
    table.insert(rolls, {
      amount = tonumber(amount),
      dice = tonumber(dice),
      monster = monster
    })
  end
  return rolls
end

interactionSystem.processNames.spawnMonsters = "Spawn Monsters"

function adventureCardSystem.launchSpawnMonstersProcess(player, rolls)
  coroutineUtils.launchAndReportErrors(
    "interactionSystem.spawnMonstersProcessCoroutine",
    function()
      interactionSystem.state[player.color].runningProcess = "spawnMonsters"
      local st, res = pcall(function()
        adventureCardSystem.spawnMonstersProcessCoroutine(player, rolls)
      end)
      interactionSystem.cleanup(player)
      if not st then error(res) end
    end
  )
end

function adventureCardSystem.spawnMonstersProcessCoroutine(player, rolls)
  local results = {}
  for i, v in ipairs(rolls) do
    if v.dice then
      if not (v.dice == 6 or v.dice == 3) then
        error(v.dice.."-sided dice not supported.")
      end
      player.broadcast(string.format("Roll for %s (%dd%d)", v.monster, v.amount, v.dice))
      log(player.color)
      log(v.amount)
      local diceRolled = interactionSystem.yieldForDice(player, v.amount, 10)
      if not diceRolled then
        player.broadcast("Roll timed out, cancelling spawn")
        return
      end
      local sumOfRolls = 0
      for _, n in ipairs(interactionSystem.state[player.color].rolls) do
        if v.dice == 3 then n = math.ceil(n / 2) end -- half if d3
        sumOfRolls = sumOfRolls + n
      end
      log(sumOfRolls)
      results[i] = sumOfRolls
    else
      results[i] = v.amount
    end
  end

  local numPlayers = #gameState.players -- metatable voodoo
  local toSpawn = {}
  for i, v in ipairs(results) do
    toSpawn[i] = math.max(1, math.floor(v * numPlayers / 4))
  end

  local monsters = monsterChest.getData().ContainedObjects

  local minis = {}
  for i, v in ipairs(rolls) do
    local monsterData = nil
    for _, m in ipairs(monsters) do
      local name = string.lower(m.Nickname)
      name = string.match(name, "^(.+) %d+g%s*$") or name
      name = string.match(name, "^%s*(.-)%s*$") or name
      if name == v.monster then
        monsterData = m
        break
      end
    end
    if monsterData == nil then
      error(string.format("No spawnable monster named %s found.", v.monster))
    end

    for i = 1, toSpawn[i] do
      table.insert(minis, monsterData)
    end
  end

  local snippets = {}
  for i, v in ipairs(rolls) do
    snippets[i] = toSpawn[i].." "..v.monster
  end
  if numPlayers == 4 then
    broadcastToAll(string.format(
      "Spawning %s.",
      table.concat(snippets, ", ")
    ), Color.Yellow)
  else
    broadcastToAll(string.format(
      "Spawning %s. (Adjusted from %s)",
      table.concat(snippets, ", "),
      table.concat(results, ", ")
    ), Color.Yellow)
  end

  local bounds = monsterSpawnArea.getBounds()
  local rows = math.ceil(#minis / (bounds.size.x / 2))
  local cols = math.min(math.floor(bounds.size.x / 2), #minis)

  for i, v in ipairs(minis) do
    local monsterObj = spawnObjectData({
      data = v,
      position = bounds.center + Vector(((i - 1) % cols + 1) * 2 - cols - 1, 1, math.ceil(i / cols) * 2 - rows - 1),
    })
    monsterUtils.initializeWoundsContextMenu(monsterObj)
  end
end

return adventureCardSystem
